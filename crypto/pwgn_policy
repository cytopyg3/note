# pwgn

sh $XDG_DATA_HOME/git/code/tools/pwgn


## master_key entry

manual (keyboard input)
visual (optical input)


## pwgn synopsis

pwgn(master_key)
sha3-512		text_block_01 &	qr_code_block
dynamic truncate	text_block_02

9ece  086e  9bac  491f  ac5c  1d10  46ca  11d7
37b9  2a2b  2ebd  93f0  05d7  b710  110c  0a67
8288  166e  7fbe  7968  83a4  f2e9  b3ca  9f48
4f52  1d0c  e464  345c  c1ae  c967  7914  9c14
███████████████████████████████████████████████
███ ▄▄▄▄▄ █▀ ▀  ██ █▄▀▄█  ▄▄▀▄▀▀▀█▄▀█ ▄▄▄▄▄ ███
███ █   █ █▀ ▄█ █▄ █ ▄██▄ █ █  █▀ ▄▀█ █   █ ███
███ █▄▄▄█ █▀ ▄▄ ▄ ▄▄█▀██▄██▄█▄▀▄█▄▀▀█ █▄▄▄█ ███
███▄▄▄▄▄▄▄█▄█▄▀▄▀ █ █ █ ▀▄█▄█ █ █▄▀▄█▄▄▄▄▄▄▄███
███▄ ▄  ▀▄  ▄ ▀▄▀█▀▄▄▄▀▄▀▄ ▄▄▀▄▀▄▀▀▀ ▀ ▀▄█▄████
████▄▄▄▄ ▄ ▄▄█▄  ▄█▄▀▄▀▀  ▀ ▄▀▄▄▄▄█▄▄▀█▀ ▄▀▄███
███▀█▄▀ █▄▄█ ▀▄▀▀▄▀█▄██▀ ██▀▄█ ▀▄▄█ ▀█ ▀▀ ▀▀███
███ ▀ █ ▀▄▀▄▄▄█▀▀▄ ███▄█▀█▄▀█▀▄▄▄▄▀▀▄█▄▀▄▀█████
███ ▄█▀ █▄▄███▀ █▄▀▄▄▀ ▀▀▀▀▀▄▄▄▀▄▄▀▀▀▀▀▀ ██████
████▄█▄  ▄▄▄▀ █ █▀▄▀█▄▀▄▄▀█▀▄▀▄▀▄▀▀▀▄█▄▄▄█▄▄███
███▄▄▀▀▄▄▄█▀█ █▄█▀▄▀▄██  ▀▄ █▀ █▄▄▀██▀ ▀▄ ▀▀███
███▀ ▀▀█▄▄▀▄█▀ ▄▀▀▄██▀█▄▀██▀█▄█▄█▄▀▄▄▀▀▄▄▀█████
███▀▄█  ▄▄▀▀ ▀  ▀█ ▀▄▄ ▄█▄▀▀▄▀▄▄▄▀▀▀▄▀▄▀██▄▄███
███ █▄▀█▀▄██▀▀█▀ ▄██▄▀█▄▄█▀ ▄▄ ▀▄▀█▄▀▀  ██ ████
███ █▄ ▄█▄▀ ██▄▀█▄█▀▄ ▄   ███▄ ▀█▄▀▀▄█▀▀▄ ▄▀███
███ ██▄█▄▄▀▄▀▀█▀█▄▄▀▄███  ▀▄▄ ▄▄█▀▀▀▀██ ▄▀▄████
███▄███▄▄▄▄ ▀▀█▀█▄ ▄ ▀ ▄█▄▀▄▄▀▄▄▄▄▀ ▄▄▄ ▄▀█████
███ ▄▄▄▄▄ █▄▄▀  █▀ ██ █▀▄█▀█▄▀ █ ▄▄ █▄█ ██▄▄███
███ █   █ █ ▀█▄▄█▀ ▀█ █  ▄▀▀██▄▀█▄▀▄ ▄▄▄ ▄▄ ███
███ █▄▄▄█ █ █  ▄▀██▀▄ █▀▀█▀▄▄▀▄▀█▄█▄▀▄▄▄▀█ ████
███▄▄▄▄▄▄▄█▄▄▄▄███▄▄█▄▄██▄██▄█▄▄▄▄▄██▄▄▄███████
███████████████████████████████████████████████
6a42  cab1  6727  ce2f  920d  af87  a7cd  2f50
bb7b  3b12  ecf7  d70b  ff4c  f681  d38f  d95b
c5e9  41f3  5e12  678f  9bfd  45be  74e5  3aad
091b  9aa3  fcab  9c4a  8c15  d9f0  33b0  75dd

default 128 bits; else 64, 32, 16, 8 or 4 bits

## interpretation legend

+---.---+
|pim|   |
.---#---.
|   | < |
+---.---+

pwgn(master_key)

    pim	r1	first four digits

    <	r8	last digit

    blocks	    (r,c)	    (r,c)
    center	pwgn(3,4 ..##)	pwgn(3,5 ##..)
		pwgn(4,4 ..##)	pwgn(4,5 ##..)
		pwgn(5,4 ..##)	pwgn(5,5 ##..)
		pwgn(6,4 ..##)	pwgn(6,5 ##..)

    aA		pwgn(7,1 ..##)	pwgn(7,2 ##..)
    2@		pwgn(8,1 ..##)	pwgn(8,2 ##..)


yss1		EOL
yss2		no EOL
pwgn(r5r6)	no EOL, no spaces
passbase64	no EOL, no spaces
center_blocks	no EOL, no spaces


## synthesize password levels
passphrase  =	$input

passbase64  =	read(bs64)

password+   =	pwgn(r5r6)  +	<   +	yss2	+   center  +	aA  +	2@  +	yss1

password    =	pwgn(r5r6)  +	<   +	yss2	+   center		    +	yss1

passwd	    =	pwgn(r5r6)  +	<   +	yss2

pswd	    =	pwgn(r5r6)

pass	    =	s							    +   yss1

pwd	    =	$HOST		    +   yss2

pw	    =				yss2				    +   yss1


## locations
Hdvp='$HOME/dock/vlt/pass'
H.lsgct='$XDG_DATA_HOME/git/code/tools'
H.lskpv='$XDG_DATA_HOME/keys/pass/vlt_pass'
% sudo cryptsetup luksUUID $H.lskpv | wl-copy -n -o
% sh $H.lsgct/vault open $H.lskpv $Hdvp		    (password)
% sh $H.lsgct/pwgn				    (pass_key)
% sh $H.lsgct/passget [$file] [$item]		    (passwd)


## cryptr sequence
pwgn(passbase64)
password
H.lsgct/cryptr [-e] file.txt [dest]
H.lsgct/cryptr [-d] file.tarx [dest]
### gnupg
cipher_cascade01: aes256	camellia256
cipher_cascade02: aes256	twofish		cast5
### openssl
cipher_cascade03: chacha20	aes-256-ctr	aes-256-cbc

## yss1
yss1	pwgn(e100)
yss1	^s	/$
yss1	wc -c	64

## yss2
yss2	pwgn(p100)
yss2	wc -c	64

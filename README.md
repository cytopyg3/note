.
|-- applications
|   |-- abook
|   |   `-- abook
|   |-- audio_prod
|   |   `-- audio_prod_install
|   |-- base16
|   |   `-- vimrc_background
|   |-- browser
|   |   |-- firefox
|   |   |   |-- custom_css
|   |   |   `-- custom_css~
|   |   |-- greasemonkey
|   |   |   `-- greasemonkey
|   |   `-- qutebrowser
|   |       `-- qutebrowser
|   |-- cal
|   |   `-- current_date_italic
|   |-- docker
|   |   |-- docker_commands
|   |   `-- docker_help
|   |-- emacs
|   |   |-- cheatsheet.org
|   |   |-- evil_mode
|   |   |-- non_m_elpa_packages
|   |   |-- org
|   |   `-- org~
|   |-- email
|   |   |-- isync
|   |   |   `-- mbsync
|   |   `-- protonmail_bridge
|   |       `-- bridge
|   |-- imagemagick
|   |   `-- imagemagick
|   |-- json
|   |   `-- processing
|   |-- kvm
|   |-- ledger
|   |   `-- csv_import_guide
|   |-- less
|   |   `-- less
|   |-- markdown
|   |   |-- cheatsheet
|   |   |-- rmarkdown
|   |   `-- test.rmd
|   |-- media
|   |   `-- images
|   |       `-- darkroom
|   |-- neatroff
|   |   `-- neatroff_install
|   |-- pacman
|   |   |-- applic_list
|   |   |   |-- applic_list
|   |   |   `-- miscellanous
|   |   |-- errors_solved
|   |   |-- offline_install
|   |   |-- pacman
|   |   |-- pacman_arch_wiki
|   |   `-- pacman_arch_wiki_tips
|   |-- suckless
|   |   |-- dwm
|   |   |   |-- config.def.h
|   |   |   `-- default_keybindings
|   |   `-- patching_rr
|   |-- tar
|   |   `-- compression
|   |-- terminal
|   |   `-- alacritty
|   |       `-- copy_paste
|   |-- tmsu
|   |   `-- tmsu
|   |-- tmux
|   |   |-- cheatsheet.tmux
|   |   |-- colors
|   |   |   |-- color_test
|   |   |   `-- xterm_color_count.sh
|   |   |-- list-keys
|   |   |-- panes
|   |   |-- resurrect_symlink
|   |   `-- tmux_resurrect_2018-03-27T18:51:32.txt
|   |-- transmission
|   |   `-- transmission
|   |-- vbox
|   |   |-- arch_guest
|   |   |-- arch_host
|   |   `-- vbox_keybindings
|   |-- vim
|   |   |-- block_select
|   |   |-- color_match
|   |   |-- copy_paste_scenarios
|   |   |-- devhints_vim
|   |   |-- folding
|   |   |-- keybindings_cheatsheet
|   |   |-- search_replace
|   |   |-- tpope_vim-surround
|   |   |-- vimcmd
|   |   |-- vim_keys_rr
|   |   |-- xterm256_color_names
|   |   `-- xterm-color-table.vim
|   |       |-- doc
|   |       |   `-- xterm-color-table.txt
|   |       |-- plugin
|   |       |   `-- xterm-color-table.vim
|   |       `-- README.markdown
|   |-- weechat
|   |   `-- weechat
|   |-- yay
|   |   |-- conflicting_packages
|   |   |-- could_not_read_db
|   |   |-- yay_D_database
|   |   |-- yay_F_files
|   |   |-- yay_P_show
|   |   |-- yay_Q_query
|   |   |-- yay_rns_wo_usr_rw
|   |   |-- yay_R_remove
|   |   |-- yay_S_sync
|   |   |-- yay_T_deptest
|   |   `-- yay_U_upgrade
|   `-- zbar
|       `-- zbarcam
|-- backup
|   `-- bu_process
|-- crypto
|   |-- 2fa
|   |   |-- 2fa_sha1_totp.sh
|   |   `-- genotp
|   |       |-- create_totp_secret_file
|   |       `-- genotp.sh
|   |-- dance.chacha20
|   |-- genpw
|   |   |-- prng
|   |   `-- pwgn.process
|   |-- gpg
|   |   |-- gnupg_set_permissions
|   |   |-- gnu_privacy_handbook
|   |   |-- gpg
|   |   |-- gpg-agent
|   |   |-- gpg_errors
|   |   |-- yk_readme
|   |   `-- yubikey
|   |-- luks
|   |   |-- boot_detach_luks_header
|   |   |-- boot_detach_luks_header2
|   |   |-- crypto_container
|   |   |-- customencrypthook
|   |   |-- luksaddkey
|   |   `-- rr_detach_luks_boot
|   |-- openssl
|   |   `-- x509_certificate_info
|   |-- password_entry
|   |-- pwgn_policy
|   |-- pwgn_policy.sig
|   |-- ssss
|   |   |-- math_ssss
|   |   |-- online
|   |   `-- shamir
|   `-- symmetric
|       |-- openssl_aes_256_cbc
|       `-- symmetric
|-- email
|   |-- mailcap
|   |-- msmtp
|   |   |-- msmtp-a-simple-mail-transfer-agent.html
|   |   `-- msmtp_notes
|   |-- neomutt
|   `-- notmuch
|-- encoding
|   |-- base64
|   |-- hex
|   `-- units_of
|-- fs
|   |-- ext234_fs
|   `-- sshfs
|-- git
|   |-- config
|   |-- del_comm_hist
|   |-- git
|   |-- git_errors
|   |-- gitignore
|   |-- rebase
|   |-- shrink
|   |-- shrinkk
|   `-- simple_guide
|-- hardware
|   |-- arduino
|   |   |-- installation
|   |   `-- no_ide
|   `-- usb
|       `-- webcam
|           `-- webcam
|-- linux
|   |-- arch
|   |   |-- boot_corrupt
|   |   |-- pacstrap
|   |   `-- structure_needs_cleaning
|   |-- dtpenv
|   |   |-- gsettings
|   |   |   `-- gsettings
|   |   |-- wayland
|   |   |   |-- backlight
|   |   |   |-- mako
|   |   |   |   |-- mako
|   |   |   |   `-- mako_h
|   |   |   `-- wayland
|   |   `-- xorg
|   |       |-- xinit_notes
|   |       `-- xorg
|   |-- lvm
|   |   `-- resize
|   |-- shell
|   |   |-- arithmatic
|   |   |-- bash
|   |   |   |-- associative_array
|   |   |   `-- README.html
|   |   |-- byte_order
|   |   |-- char_classes
|   |   |-- coding_standards
|   |   |-- color
|   |   |   |-- 16m_color_capability
|   |   |   |-- 16m_color_table
|   |   |   |-- 256_color_table
|   |   |   |-- colorr
|   |   |   |-- from_hex
|   |   |   `-- to_hex
|   |   |-- command
|   |   |   |-- alias
|   |   |   |-- awk
|   |   |   |   |-- awk_rr
|   |   |   |   |-- format_columns
|   |   |   |   |-- shell_vars_in_awk
|   |   |   |   `-- techmint
|   |   |   |       `-- awk_techmint
|   |   |   |-- bc
|   |   |   |   `-- floating_point_arithmatic
|   |   |   |-- case
|   |   |   |-- catalog
|   |   |   |-- cheatsheet
|   |   |   |   |-- 7z
|   |   |   |   |-- aaa_source
|   |   |   |   |-- ab
|   |   |   |   |-- alias
|   |   |   |   |-- ansi
|   |   |   |   |-- apk
|   |   |   |   |-- apparmor
|   |   |   |   |-- apt
|   |   |   |   |-- apt-cache
|   |   |   |   |-- apt-get
|   |   |   |   |-- aptitude
|   |   |   |   |-- aria2c
|   |   |   |   |-- asciiart
|   |   |   |   |-- asterisk
|   |   |   |   |-- at
|   |   |   |   |-- awk
|   |   |   |   |-- bash
|   |   |   |   |-- bower
|   |   |   |   |-- bzip2
|   |   |   |   |-- cat
|   |   |   |   |-- cd
|   |   |   |   |-- cheat
|   |   |   |   |-- chmod
|   |   |   |   |-- chown
|   |   |   |   |-- comm
|   |   |   |   |-- convert
|   |   |   |   |-- cp
|   |   |   |   |-- cpdf
|   |   |   |   |-- crontab
|   |   |   |   |-- cryptsetup
|   |   |   |   |-- csplit
|   |   |   |   |-- cups
|   |   |   |   |-- curl
|   |   |   |   |-- cut
|   |   |   |   |-- date
|   |   |   |   |-- dd
|   |   |   |   |-- deb
|   |   |   |   |-- df
|   |   |   |   |-- dhclient
|   |   |   |   |-- diff
|   |   |   |   |-- distcc
|   |   |   |   |-- dnf
|   |   |   |   |-- docker
|   |   |   |   |-- dpkg
|   |   |   |   |-- du
|   |   |   |   |-- emacs
|   |   |   |   |-- export
|   |   |   |   |-- ffmpeg
|   |   |   |   |-- find
|   |   |   |   |-- fkill
|   |   |   |   |-- for
|   |   |   |   |-- gcc
|   |   |   |   |-- gdb
|   |   |   |   |-- git
|   |   |   |   |-- gpg
|   |   |   |   |-- grep
|   |   |   |   |-- gs
|   |   |   |   |-- gyb
|   |   |   |   |-- gzip
|   |   |   |   |-- hardware-info
|   |   |   |   |-- head
|   |   |   |   |-- hg
|   |   |   |   |-- history
|   |   |   |   |-- http
|   |   |   |   |-- hub
|   |   |   |   |-- iconv
|   |   |   |   |-- ifconfig
|   |   |   |   |-- indent
|   |   |   |   |-- ip
|   |   |   |   |-- iptables
|   |   |   |   |-- irssi
|   |   |   |   |-- iwconfig
|   |   |   |   |-- journalctl
|   |   |   |   |-- jq
|   |   |   |   |-- jrnl
|   |   |   |   |-- kill
|   |   |   |   |-- less
|   |   |   |   |-- lib
|   |   |   |   |-- ln
|   |   |   |   |-- ls
|   |   |   |   |-- lsblk
|   |   |   |   |-- lsof
|   |   |   |   |-- lvm
|   |   |   |   |-- man
|   |   |   |   |-- markdown
|   |   |   |   |-- mdadm
|   |   |   |   |-- mkdir
|   |   |   |   |-- more
|   |   |   |   |-- mount
|   |   |   |   |-- mutt
|   |   |   |   |-- mv
|   |   |   |   |-- mysql
|   |   |   |   |-- mysqldump
|   |   |   |   |-- nc
|   |   |   |   |-- ncat
|   |   |   |   |-- ncdu
|   |   |   |   |-- netstat
|   |   |   |   |-- nkf
|   |   |   |   |-- nmap
|   |   |   |   |-- nmcli
|   |   |   |   |-- notify-send
|   |   |   |   |-- nova
|   |   |   |   |-- npm
|   |   |   |   |-- ntp
|   |   |   |   |-- numfmt
|   |   |   |   |-- od
|   |   |   |   |-- Online color picker for terminal 256 color palette. Examples for shell prompt, vim, term_xterm_files
|   |   |   |   |   |-- appendix_a.js
|   |   |   |   |   |-- colors.js
|   |   |   |   |   |-- jquery-3.js
|   |   |   |   |   |-- main.css
|   |   |   |   |   `-- main.js
|   |   |   |   |-- Online color picker for terminal 256 color palette. Examples for shell prompt, vim, term_xterm.html
|   |   |   |   |-- openssl
|   |   |   |   |-- org-mode
|   |   |   |   |-- p4
|   |   |   |   |-- pacman
|   |   |   |   |-- paste
|   |   |   |   |-- patch
|   |   |   |   |-- pdftk
|   |   |   |   |-- perl
|   |   |   |   |-- pgrep
|   |   |   |   |-- php
|   |   |   |   |-- ping
|   |   |   |   |-- ping6
|   |   |   |   |-- pip
|   |   |   |   |-- pkcon
|   |   |   |   |-- pkgtools
|   |   |   |   |-- pkill
|   |   |   |   |-- popd
|   |   |   |   |-- ps
|   |   |   |   |-- psql
|   |   |   |   |-- pushd
|   |   |   |   |-- pwd
|   |   |   |   |-- python
|   |   |   |   |-- r2
|   |   |   |   |-- rcs
|   |   |   |   |-- readline
|   |   |   |   |-- rename
|   |   |   |   |-- rm
|   |   |   |   |-- route
|   |   |   |   |-- rpm
|   |   |   |   |-- rpm2cpio
|   |   |   |   |-- rss2email
|   |   |   |   |-- rsync
|   |   |   |   |-- sam2p
|   |   |   |   |-- scd
|   |   |   |   |-- scp
|   |   |   |   |-- screen
|   |   |   |   |-- sed
|   |   |   |   |-- shred
|   |   |   |   |-- shutdown
|   |   |   |   |-- slurm
|   |   |   |   |-- smbclient
|   |   |   |   |-- snap
|   |   |   |   |-- snmpwalk
|   |   |   |   |-- socat
|   |   |   |   |-- sockstat
|   |   |   |   |-- sort
|   |   |   |   |-- split
|   |   |   |   |-- sport
|   |   |   |   |-- sqlite3
|   |   |   |   |-- sqlmap
|   |   |   |   |-- ss
|   |   |   |   |-- ssh
|   |   |   |   |-- ssh-copy-id
|   |   |   |   |-- ssh-keygen
|   |   |   |   |-- stdout
|   |   |   |   |-- strace
|   |   |   |   |-- su
|   |   |   |   |-- svn
|   |   |   |   |-- systemctl
|   |   |   |   |-- systemd
|   |   |   |   |-- tail
|   |   |   |   |-- tar
|   |   |   |   |-- tcpdump
|   |   |   |   |-- tee
|   |   |   |   |-- tidy
|   |   |   |   |-- tmux
|   |   |   |   |-- top
|   |   |   |   |-- tr
|   |   |   |   |-- trashy
|   |   |   |   |-- tree
|   |   |   |   |-- truncate
|   |   |   |   |-- udisksctl
|   |   |   |   |-- uname
|   |   |   |   |-- uniq
|   |   |   |   |-- unzip
|   |   |   |   |-- urpm
|   |   |   |   |-- vagrant
|   |   |   |   |-- vim
|   |   |   |   |-- virtualenv
|   |   |   |   |-- wc
|   |   |   |   |-- weechat
|   |   |   |   |-- wget
|   |   |   |   |-- xargs
|   |   |   |   |-- xmlto
|   |   |   |   |-- xrandr
|   |   |   |   |-- xxd
|   |   |   |   |-- yaourt
|   |   |   |   |-- youtube-dl
|   |   |   |   |-- yum
|   |   |   |   |-- z
|   |   |   |   |-- zfs
|   |   |   |   |-- zip
|   |   |   |   |-- zoneadm
|   |   |   |   `-- zsh
|   |   |   |-- find
|   |   |   |   `-- find
|   |   |   |-- iconv
|   |   |   |-- ifs
|   |   |   |-- mount
|   |   |   |-- mount_cifs
|   |   |   |-- nmcli
|   |   |   |-- printf
|   |   |   |-- read
|   |   |   |   |-- filedescriptor
|   |   |   |   `-- read_into_array
|   |   |   |-- rm
|   |   |   |   `-- undelete
|   |   |   |-- rsync
|   |   |   |-- sed
|   |   |   |   |-- gistfile1.txt
|   |   |   |   |-- sed_101
|   |   |   |   |-- sed_cheat_sheet
|   |   |   |   |-- sed_guide.url
|   |   |   |   `-- swap_columns_sed
|   |   |   |-- set
|   |   |   |-- stat
|   |   |   |-- sudo
|   |   |   |-- tar
|   |   |   |-- tput
|   |   |   |   `-- tput
|   |   |   |-- tr
|   |   |   |   `-- tr
|   |   |   |-- while
|   |   |   `-- yay
|   |   |       |-- yay_D_database
|   |   |       |-- yay_F_files
|   |   |       |-- yay_P_show
|   |   |       |-- yay_Q_query
|   |   |       |-- yay_R_remove
|   |   |       |-- yay_S_sync
|   |   |       |-- yay_T_deptest
|   |   |       `-- yay_U_upgrade
|   |   |-- comparison
|   |   |   |-- file_test_operators
|   |   |   |-- integer_test_operators
|   |   |   `-- string_test_operators
|   |   |-- conditions
|   |   |   |-- command_substitution
|   |   |   |-- Conditions_in_bash_Linux_Academy.txt
|   |   |   |-- for_loop
|   |   |   |-- if_conditions
|   |   |   |-- parameters
|   |   |   `-- while_loop
|   |   |-- console_terminal
|   |   |-- copy_paste
|   |   |-- devhints_bash
|   |   |-- device_drivers
|   |   |-- dirs_pushd_popd
|   |   |-- exit_codes
|   |   |-- expansion
|   |   |   |-- expansion_types
|   |   |   `-- parameter_expansion
|   |   |-- file_permissions
|   |   |-- fonts
|   |   |-- getopts
|   |   |-- getopts_web
|   |   |-- globbing
|   |   |-- hardware_info
|   |   |-- here_strings
|   |   |-- locale
|   |   |-- password_policy
|   |   |-- pure-sh-bible.html
|   |   |-- quotation
|   |   |-- standard_streams
|   |   |-- std_cmd_flags
|   |   |-- string_manipulation
|   |   |-- terminal_escape_sequences
|   |   |-- tty
|   |   |-- users_groups
|   |   `-- zsh
|   |       |-- last_command_get
|   |       |-- multiline_comment
|   |       |-- shell_buildin_commands
|   |       |-- shell_types
|   |       |-- zle_zsh_line_editor.html
|   |       |-- zsh_bindkey
|   |       |-- zsh_config_example
|   |       `-- zsh_git_prompt
|   |-- systemd
|   |   |-- boot
|   |   |-- journalctl
|   |   |-- localectl
|   |   |-- runlevel
|   |   |-- shutdown
|   |   |-- systemd
|   |   |-- timers
|   |   |-- troubleshooting
|   |   |-- units
|   |   `-- unit_timer_test
|   |       |-- unit_timer_test_manual
|   |       |-- unit_timer_test.service
|   |       |-- unit_timer_test.sh
|   |       `-- unit_timer_test.timer
|   `-- xdg
|       |-- freedesktop
|       |-- mime_type
|       |-- rfc1524.html
|       |-- rfc822.html
|       `-- specifications
|           `-- base_directory_spec
|-- logic
|   |-- bitsnbytes
|   |-- gates
|   `-- operators
|-- maths
|   |-- algebra.ms
|   |-- algebra.ms.pdf
|   |-- default.mom
|   |-- default.mom.pdf
|   |-- exp2c_1.ms
|   |-- exp2c_1.pdf
|   |-- exp2c_2.ms
|   |-- exp2c_2.pdf
|   |-- exp2c_3.ms
|   |-- exp2c_3.pdf
|   |-- exp2c_4.ms
|   |-- exp2c_4.pdf
|   |-- exp2c_5.mom
|   |-- exp2c_5.mom.pdf
|   |-- exp2c_5.ms
|   |-- exp2c_5.ms.pdf
|   |-- formulas.ms.pdf
|   |-- geometry.ms
|   |-- groff_template.ms
|   |-- number_e
|   |-- number_pi
|   |-- number_theory.ms
|   |-- number_theory.ms.pdf
|   |-- route_plotting
|   |-- test2.mom
|   |-- test2.mom.pdf
|   |-- test.ms
|   `-- test.ms.pdf
|-- network
|   |-- bluetooth
|   |   |-- bluetoothctl
|   |   `-- var_lib_bluetooth
|   |-- dns
|   |   |-- dig
|   |   |-- records
|   |   `-- resolvctl
|   |-- etc_netctl_examples
|   |   |-- bonding
|   |   |-- bridge
|   |   |-- ethernet-custom
|   |   |-- ethernet-dhcp
|   |   |-- ethernet-static
|   |   |-- macvlan-dhcp
|   |   |-- macvlan-static
|   |   |-- mobile_ppp
|   |   |-- openvswitch
|   |   |-- pppoe
|   |   |-- tunnel
|   |   |-- tuntap
|   |   |-- vlan-dhcp
|   |   |-- vlan-static
|   |   |-- wireless-open
|   |   |-- wireless-wep
|   |   |-- wireless-wpa
|   |   |-- wireless-wpa-config
|   |   |-- wireless-wpa-configsection
|   |   `-- wireless-wpa-static
|   |-- etc_systemd_network
|   |   |-- eno1.network
|   |   |-- systemd-resolved
|   |   `-- todo
|   |-- hosts
|   |   |-- adblocking_etc_hosts
|   |   `-- adblocking_etc_hosts~
|   |-- internet
|   |   |-- ipv6
|   |   `-- user_agent
|   |-- iptables
|   |   `-- cytopyge_fw_rules.sh
|   |-- mifi_over_usb
|   |   `-- hw_e5786
|   |       |-- 12d1:1506.conf
|   |       |-- 40-usb_modeswitch.rules
|   |       `-- README.md
|   |-- nmcli
|   |-- tor
|   |   `-- qb_tor
|   |-- tshark
|   |   |-- loc_regex
|   |   |-- moloch_install
|   |   |-- pcap_analysis
|   |   |-- pcap_analysis_OLD
|   |   |-- report
|   |   |-- tshark
|   |   |-- user_config
|   |   `-- wireshark
|   |-- ubiquity
|   |   |-- config.boot
|   |   |-- README.md
|   |   |-- Ubiquiti
|   |   |   |-- edgeos_ubnt_20170221.tar.gz
|   |   |   |-- README.md
|   |   |   |-- show configuration commands.txt
|   |   |   |-- Ubiquiti Home Network.pdf
|   |   |   `-- Ubiquiti Project Brochure.pdf
|   |   `-- Ubiquiti Home Network.pdf
|   |-- uri
|   |-- urls
|   |   `-- search_uris
|   `-- vpn
|       |-- fortisslvpn
|       |   `-- fortisslvpn
|       |-- openvpn
|       |   `-- openvpn
|       `-- protonvpn
|           |-- native_client
|           |-- openvpn_client
|           |-- protonvpn
|           `-- protonvpn_cli
|-- opsys
|   |-- android
|   |   |-- fastboot
|   |   `-- file_transfer
|   `-- windows
|       `-- of_course_nothing_here
|-- osint
|   |-- cd
|   |-- intelligence
|   `-- phone_number
|-- parsers
|   |-- json
|   |   `-- json_prettify
|   |-- xml
|   |   `-- xml_prettify
|   `-- youtube-dl
|       |-- clone_yt.sh
|       |-- make_clone_yt.sh
|       |-- README.md
|       `-- yt_comments.py
|-- perl
|   `-- perl_101
|-- radio
|   |-- bfuv5r
|   |-- koncept
|   |-- morse
|   |-- morse_encode
|   |-- morse_mnemonics2.png
|   |-- morse_mnemonics.png
|   `-- sstv
|       `-- testbeeld.jpg
|-- README.md
|-- regex
|   |-- grep
|   |-- meta_chars
|   |-- quick_cheat_sheet
|   |-- regexr
|   |-- top_regex
|   `-- urls
|-- security
|   |-- bw
|   |   `-- bw_reference
|   |-- pass
|   |   |-- manpage
|   |   `-- pass
|   `-- yubikey
|       |-- ykman
|       |-- yk_u2f
|       `-- yubikey4
|-- sv
|   `-- grammatik
|-- ucode
|   `-- ucode
`-- zzz_deprecated
    `-- xab

132 directories, 586 files

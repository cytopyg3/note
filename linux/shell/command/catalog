#!/usr/bin/env bash  ## search $path for bash
#: '\nmulti\nline\ncode\nblock\n# '  ## switchable multiline codeblock / comment (remove first #)
< ${$(get_newest_file $XDG_LOGS_HOME/updater/$HOST/package_lists/)%?}Q  ## make last character of newest file a Q and print that file
< /home/cytopyge/.config/qutebrowser/bookmark_urls | fzf | slicer '(' ')'  ## get from bookmark_urls
<command> &> <file>  ## <command> stdout to <file>
<command> 2>&1 | tee -a logfile  ## <command> stderr to stdout, also append to logfile
<command> >/dev/null 2>&1  ## silent execution; suppress all output from command
((i++))  ## increment i by one (i.e. in a loop)
((i--))  ## decrement i by one (i.e. in a loop)
amixer info  ## info about alsamixer device
amixer scontrols  ## list of alsamixer controls
amixer set Capture toggle	## alsamixer set
amixer set Capture volume 0 nocap
amixer set Internal Mic Capture volume 0 nocap
amixer set Master playback volume 30 unmute
apropos keyword  ## search man page names and descriptions
awk '!($1="")' <file>  ## remove column 1
basename <pathname>  ## print <pathname> without any leading directory components
bindkey  ## zsh bound keys
blkid /dev/sda128 | awk '{print $3}' | cut -d '"' -f 2  ## get uuid from /dev/sd
bluetoothctl connect $(bluetoothctl devices | fzf | awk '{print $2}') && bluetoothctl info  ## bluetooth connect
bluetoothctl disconnect $(bluetoothctl devices | fzf | awk '{print $2}') && bluetoothctl info  ## bluetooth disconnect
bluetoothctl discoverable off	## bluetooth not broadcast radio device
bluetoothctl power off && sudo systemctl stop bluetooth.service && sudo rfkill block bluetooth  ## bluetooth radio off
cal -3 -m -w `date -I`  ## calendar showing last, current and next month
cal -y -m -w `date +%Y`	 ## current year calendar
cat ap.wifi | grep #psk | awk -F "\"" '{print $2}' | qrencode -t utf8  ## wifi accesspoint password qr
cat -n <file>  ## line number cat
cat -vE $file  ## prevent cat shell injection
cat 0< $file >1 /dev/stdout  ## rudimentary form of: cat $file
cat <<- EOF >> file\n multi\nline\ntext\nmlt\nEOF\n  ## append mlt to file (heredoc); strip leading tabs from mlt (-); EOF can be any unique limit string
cd "$(fd -t d -H -I . $HOME | fzf --preview="tree -L 1 {}" --bind="space:toggle-preview" --preview-window=:hidden)"  ## cd with fzf from $HOME, spacebar for info box
cd $HOME/.config/hosts; git switch blocklist_on; cd -  ## activate network blocklist
cd $XDG_CONFIG_HOME/shln; sh chln; cd .; cd -  ## change symlinks to current $USER for $PATH
cd /usr/lib/modules/$(uname -r)
cd /var/lib/pacman/local  ## pacman install directory
chattr -R +i file  ## 'i' attribute: file cannot be modified
chmod --reference=ref_file target  ## make target file mode bits same as ref_file
chown -R user:group directory	## recursive set user & group ownership
chown :group file	## set group ownership
chown user: file	## set user ownership
clear  ## shift current line to top of screen
cp -prv --attributes-only  ## copy no content, but only mode, ownership and timestamps, verbose
cp -pv  ## copy and preserve mode, ownership and timestamps, verbose
cryptsetup --cipher=aes-xts-plain64 -hash=sha512 --key-size=512 --offset=0 --verify-passphrase open --type=plain /dev/sdX lvm
cryptsetup --cipher=aes-xts-plain64 -hash=sha512 --key-size=512 --offset=0 key-file=/dev/sdZ open --type=plain /dev/sdX lvm
cryptsetup --cipher=serpent-xts-plain -hash=sha512 --key-size=512 --iter-time=4096 --use-random --verify-passphrase luksFormat /dev/sdX
cryptsetup luksAddKey --key-slot 7 $file  ## add luks key to slot 7 of $file
cryptsetup luksRemoveKey --key-slot 7 $file  ## remove luks key from slot 7 of $file
cryptsetup luksDump file  ## dump header information
cryptsetup luksChangeKey file  ## change a key
cryptsetup luksUUID <luks_container> | wl-copy -o -n  ## get luks container id
cryptsetup open /dev/sdX cryptroot
cryptsetup open /dev/sdX cryptroot --debug  ## which keyslot is used for passphrase
ctags -R ~/.
curl $(wl-paste) | zathura -  ## stream a copied (qb ;y) pdf url
curl -4 https://ident.me	## external ip address
curl -O https://mirror.i3d.net/pub/archlinux/iso/2018.04.01/archlinux-2018.04.01-x86_64.iso | sudo dd of=/dev/sdX	## download arch linux iso and write to device
date && sudo hwclock --show --verbose  ## read hardware clock time and its drift
date +%s  ## epoch current date
date -d @1234567890 +%Y%m%d_%H%M%S_%N_%z_%Z_%V	## convert epoch to date time
date -u  ## current zulu time
dd if=/dev/sdX of=/dev/sdY bs=4M status=progress oflag=sync	## dd copy sdX to sdY
df -h	## file system information human-readable
dhcpcd -k wlp58s0  ## dhcp remove interface ip
dhcpcd wlp58s0	## dhcp request for interface
diff -Naur <lf> <rf> > patch_file  ## recursive diff from lf to rf with 3 lines of context (unified)
dig -x 89.39.107.195  ## reverse lookup own public ip
dirname <filename>  ## print <filename> without last component
du -sbh <directory>  ## total size of all content in directory
e2label /dev/X [label]  ## read [/rewrite] device label
echo "obase=02; ibase=16; $(echo $hex | tr a-z A-Z)" | bc  ## bc hexadecimal to binary conversion
echo "obase=10; ibase=16; $(echo $hex | tr a-z A-Z)" | bc  ## bc hexadecimal to decimal conversion
echo "obase=16; ibase=10; $dec" | bc  ## bc dec (10) to hex (16) conversion
echo -n $string | base64  ## base64 encoding
echo 0  ## print exit code of the last executed command
echo ${USER%??}$string  ## replace last two (%??) characters of $USER by $string
env  ## print current environment variables and their values
faillock --user $USER --reset  ## remove sudo lockout for current user
fc  ## edit last command in
fc -l -n -1  ## last command entered, hide line number
fc-list  ## list available fonts
fg [%n]  ## job [n] to foreground
fg [id]  ## recall suspended command to foreground
figlet -f mini cytopyge | wl-copy  ## figlet sub_header
figlet text | wl-copy  ## figlet header
find . -exec <command> {} \;
find . -maxdepth 1 -name \*_ -type d -exec cp -r {} ~/test_/bu \;
find . -maxdepth 1 -type d -exec du -sb {} \; | sort -g -k 1  ## directories sorted by size (bytes)
find . -maxdepth 1 -type l -exec cp -d {} $HOME/.dot \;  ## find symlinks and copy without dereferencing
find . -name filename_start__\*
find . -print -exec cat {} \; -printf n
find . -type f ! -newermt "2018-12-31" -delet3	## find and delete all files older (not newer) than date
find . -type f -exec mv {} <dest> \;  ## find all files and move them to destination directory
find . -type l -exec sh -c "file -b {} | grep -q ^broken" \; -print
find . -xtype l -print
find ./ -maxdepth 1 -type f -mtime +1 -print
find / -iname file_case_insensitive
find /<directory> -name '<search-string>' 2>/dev/null
find /<directory> -type f -exec shred -v -z -n 3 -u {} \;
findmnt
for file in *; do cp -- "$file" "string_$file"; done  ## append string_ to each file in cwd
for i in {0..255}; do; printf "\x1b[38;5;${i}mcolour${i}\x1b[0m\n"; done  ## print 256 colors
for i in {1..65535}; do (echo < /dev/tcp/127.0.0.1/$i) &>/dev/null && printf "\n[+] open port\n: \t%d\n" "$i" || printf "$i\r"; done  ## portscan
for i in {0..7}; do; printf "\033[0;3${i}mcolor$i\n"; done  ## print 8 colors
free -m  ## free and used memory in system
fuser -vmM <file(system)>	## display PIDs using file / file(system) (filesystem in mount)
fusermount -u <mountpoint>  ## umount simple-mptfs android device
getent ahosts <url>  ## get ip address from name service switch library database ahost
git config --show-scope --show-origin --list  ## origin and scope of git configuration
git log --oneline  ## list of commits
git reset --hard 2db93df  ## back to a previous commit
glances
gnutls-cli -d 1 grc.com -p 443 < /dev/null  ## get certificate info
gpg --armor --export ID > ID.puk.gpg  ## export public key ascii armored id
gpg --detach-sign --local-user 0x12345678 $file  ## sign $file with specific secret key
gpg --edit-key ID >  ## edit public key
gpg --export ID > ID.puk.gpg  ## export public key binary
gpg --fingerprint | grep -i -B 1 <uid/email> | head -n 1 | awk -F = '{print $2}' | sed ':a;N;$!ba;s/\n//g' | sed 's/ //g' | sed 's/\r$//g'  ## get gpg fingerprint w/o spaces from uid (email)
gpg --expert --full-generate-key  ## generate new keypair
gpg --import ID >  ## import public key
gpg --list-keys | grep ^uid | sed s'/  //g' | fzf | awk '{print $NF}' | sed 's/<//' | sed 's/>//' | xargs -I {} sh -c "gpg --list-keys | grep -B 1 {}" | head -n 1 | awk -F = '{print $2}' | sed ':a;N;$!ba;s/\n//g' | sed 's/ //g' | sed 's/\r$//g'  ## get fingerprint from fzf uid list
gpg --output ID.puk.asc --armor --export ID >  ## export public key ascii armored
gpg --output file --decrypt file.gpg >  ## decrypt file.gpg asymmetric using private key
gpg --output file.gpg --encrypt --recipient ID file >  ## encrypt file asymmetric using public key ID
gpg --output file.gpg --symmetric file >  ## encrypt file symmetric
gpg -e -a -r <id> file ## encrypt file armored for recipient <id> (like pass)
gpg --output revoke.asc --gen-revoke ID  ## generate revocation certificate
gpg -vv public_key.asc
gpg-connect-agent reloadagent /bye  ## clear passphrases gpg cache
gpg-connect-agent 'keyinfo --list' /bye 2>/dev/null | awk 'BEGIN{CACHED=0} /^S/ {if($7==1){CACHED=1}} END{if($0!=""){print CACHED} else {print "0"}}'  ## gpg cache status (1=cached)
grep "\sro[\s,]" /proc/mounts
grep -riIl 'text' [directory]  ## find files (no binary) containing text recursively in directory
grep -riIn 'text' [directory]  ## find files (no binary) containing text recursively in directory with linenumbers
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=out.pdf in1.pdf in2.pdf in3.pdf  ## concatenate pdf files
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=out.pdf -dFirstPage=a -dLastPage=b $infile.pdf  ## extract pages a through b from $infile.pdf
history -d <history_id>
history -t +%Y%m%d_%H%M%S
hmac=$(printf $input | openssl dgst -sha512 -hmac $input | sed 's/^.*= //')  ## zz2
host -t A <ip_addr/domain>  ## dns lookup A record
host -v -t ANY <ip_addr/domain>  ## dns lookup any type verbose output
hwclock --set --date='01/01/2010 00:00:00'; sudo hwclock --hctosys  ## set date and time
id -u [$USER]  ## show effective user id
id -g [$USER]  ## show effective group id
id -un [$USER]  ## show effective user name
id -gn [$USER]  ## show effective group name
id [$USER]  ## show both real and effective uid & gid
iftop -i <interface> ## display bandwith usage of interface
info  ## advanced manual system primarily for gnu programs
insmod <filename> [args]  ## insert module into kernel (if not in /usr/lib/modules/$(uname -r))
ip a add 192.168.1.200/255.255.255.0 dev <interface>	## assign ip address to interface
ip a add <ip>/<subnet_mask> broadcast <broadcast_ip> dev <interface>
ip a del 192.168.1.200/24 dev <interface>	## remove ip address from interface
ip a flush <interface>	## remove all ip addresses from interface
ip link set <interface> {down/up}   ## change interface state
ip link set dev [interface] address [mac]  ## assign [mac] address to [interface]
ip link set dev [interface] down  ## disconnect from the network by disable interface
ip n show	## show neighbour / arp cache
ip route add default via <default_gateway>
ip route flush dev <interface>
ipset add blocklist [ipaddrs]  ## add [ipaddrs] to blocklist
ipset create blocklist hash:ip  ## create ip blocklist
iw dev [wl_interface] scan | grep SSID | sort | uniq  ## received SSID list
jobs  ## list background processes
journalctl --list-boots
journalctl --vacuum-size=500M
journalctl -b, --boot[ID]
journalctl -e
journalctl -f, --follow
journalctl -k
journalctl -o json-pretty
journalctl -o verbose
journalctl -p 3 -x
journalctl -u <unit>
journalctl _<tab>
kill $(pidof <process_name>)
ln -s <target> <link_name>  ## create sym<link_name> to <target>
ln -s -f <target> <link_name>  ## replace sym<link_name> to <target>
losetup -l -a  ## list all loop devices
losetup -d $loop_dev  ## detach $loop_dev
ls $XDG_DATA_HOME/keys/wl | fzf | xargs -r cat | grep #psk | awk -F "\"" '{print $2}' | qrencode -t utf8  ## fzf select wifi key and show qr
lsblk -i --tree -o name,fstype,uuid,path,size,fsuse%,fsused,label,mountpoint  ## list block devices
lsblk -o fsuse%,path,fsused,size | grep '%' | awk 'NR == 1; NR > 1 {print $0 | "sort -nr"}'  ## block devices usage percentage
lscpu  ## display cpu architecture
lshw  ## list hardware configuration
lsmod  ## list drivers currently added to kernel
lsof +f -- [path/1 path/2]  ## list open files in paths opened by processes
lsof -p [PID]  ## list open files for PID
lspci  ## list pci devices
lspci -k -d #[<vendor>]:[<device>][:<class>]
lspci -k -s <xx:xx.x> #[[[[<domain>]:]<bus>]:][<slot>][.[<func>]]
lspci -kvvt
lspci -vv  ## list pci devices, find kernel drivers
man -k . | fzf | awk '{print $1}' | xargs -r env TERM=xterm man  ## fzf find and open manpage
man zshall  ## view the system reference manual for all zsh
metar get
metar set eham
mkdir /dock/<1-4>
nmcli connection add type ethernet con-name $con_name ifname $if_name
nmcli connection modify $con_name ipv4.addresses 192.168.0.39/24 ipv4.gateway 192.168.2.2 ipv4.dns 192.168.2.2 +ipv4.dns 9.9.9.9 connection.autoconnect no ipv4.method manual
modprobe -r <driver>  ## remove driver from kernel
modprobe <driver>  ## add driver to kernel
mount -L $label $mountpoint  ## mount device with $label to $mountpoint
mount -o remount,rw /dev/sdY1 /boot  ## remount rw existing mountpoint
mount -o uid=$(whoami),gid=$(id -gn) /dev/sdX dock/1  ## mount as current user
mount -t tmpfs tmpfs /mnt/ramdisk -o size=4096  ## create ramdisk
mount | grep ro,  ## find all ro mountpoints
mpv --start=+00:00 --ytdl-format='best[filesize<300M]' --profile=cache_xs --demuxer-cache-wait --msg-level=all=v --http-header-fields="User-Agent: $(head -n 1 $USER_AGENT)" $(wl-paste) ## stream movie verbose with mpv.conf cache_s profile
mpv `echo $url | sed 's/https:\/\/www.youtube.com\/watch?v=/https:\/\/youtu.be\//'`  ## change yt url
msgcat --color=test
mv -t <target_dir> <source_dirs>  ## move source_dirs to target_dir
notmuch address '*'  ## all mail addresses
nslookup server.com  ## get server ip address
openssl enc -d -base64 -aes-256-gcm -pbkdf2 -salt -in crypt.aes -out plain.txt  ## openssl base64 encoded aes 256 bits galois counter mode decryption
openssl enc -e -base64 -aes-256-gcm -pbkdf2 -salt -in plain.txt -out crypt.aes  ## openssl base64 encoded aes 256 bits galois counter mode encryption
openssl s_client -connect grc.com:443 -showcerts < /dev/null | openssl x509 -outform pem > cert.pem  ## write x509 public key certificate from grc.com:443 as pem to file cert.pem
openssl s_client -connect $(wl-paste -n):443 -showcerts < /dev/null | openssl x509 -noout -text ## print x509 public key certificate as text to stdout
openssl s_client -connect $server_ip:443 -servername $server_name -showcerts  ## print x509 public key certificate from $(wl-paste -n):443 as text to stdout with server name indication (SNI) in the clienthello message (if a reverse proxy ip serves multiple webservers)
openssl x509 -in cert.pem -noout -text  ## print x509 public key certificate from file cert.pem as text to stdout
openvpn --config openvpn/udp-ams1_udp.ovpn
output=$(printf $input | openssl dgst -sha512 -hmac $hmac | sed 's/^.*= //') ## zz3
pacman-key --delete <pub_key>
pacman-key --finger <pub_key>
pacman-key --lsign-key <pub_key>
pactree -r [package]  ## show [package] dependant tree, reverse depedencies, packages that depend on [package]
pactree <package>  ## show [package] dependency tree
patch -p0 < patch_file ##  apply patch_file (see diff)
pim2fa=$(printf $input | openssl dgst -sha512 -hmac pim2fa | sed 's/^.*= //') ## zz4
pim=$(printf $input | xxd -p)  ## zz4_OLD
ping -D -i 10 -c 5 9.9.9.9
pkill wpa_supplicant && sudo dhcpcd -k wlp58s0 && sudo pkill dhcpcd && sudo ip a flush wlp58s0
printf "$tring" | awk '{print $1+0}'  ## awk retrieves the second field as a number (+0) from $tring, anything after the number will be ignored
printf "%03d%s" "2"  ## print leading zeros
printf "%3d%s" "2"  ## print leading spaces
printf "%d\n" \'A  ## character (A) to ascii value (65)
printf "Mime-Version: 1.0; Content-Type: text/plain; charset=us-ascii; Content-Transfer-Encoding: 7bit; Subject: <subject>\n; <body>" | msmtp -v -C <msmtp_config> -a <profile_name> -- destination@mail.com
printf "\x$(printf %x 65)"  ## ascii value (65) to character (A)
printf "space_every_4th_char" | fold -w 4 | paste -sd ' ' -
printf "space_every_4th_char" | sed 's/.\{4\}/& /g'
printf $(( [#n] x ))  ## print base 10 value of x in base n, with 2<=n<=36
printf text | openssl dgst -sha512 -hmac key | sed 's/^.*= //' | wl-copy
ps -ajxf  ## print a process tree
ps -eFly  ## list every process on the system
ps -eo %cpu,%mem,pid,pgid,user,state,start,args= --sort=-etimes  ## list processes custom AIX format descriptors, sort low to high elapsed time since process was started
ps -eo %cpu,%mem,pid,user,command= --sort=-%cpu  ## list processes custom AIX format descriptors, sort high to low cpu usage
pstree -ptg  ## display a tree of processes, show PIDs, PGIDs and full names for threads
qrencode -t utf8  ## create qr from stdin
qutebrowser -s content.canvas_reading true ## qb for windy
rankmirrors -n 10 /etc/pacman.d/mirrorlist | grep -w 'Server =' >> mirrorlist.ranked
rcvboxdrv
read input  ## zz1
read -r input; printf '%s' "$input" | base64 | wl-copy -n -o; unset input  ## input base64 translation
read -r input; printf '%s' "$input" | openssl dgst -sha3-512 | sed 's/^.*= //'; input=; unset input  ## secure sha3-512 hash from stdin
readlink <file>  ## print resolved symbolic links or canonical file names
readlink <symlink> | cut -d / -f 4- | awk -v home="$HOME/" '{print home $0}'  ## change symlink to current users $HOME
realpath file  ## return canonicalized absolute pathname
renice -19 $(pgrep <process_name>)  ## give <process_name> highest (-19) priority
reset  ## reinitialize terminal state
rev $file  ## reverse line characters
rfkill  ## check hard and soft block status wireless radios
rfkill [un]block bluetooth  ## soft [un]block bluetooth radio
rfkill [un]block wlan  ## soft [un]block wifi radio
rm -rf -- '-r'
rmmod btusb
rsync -aAXv --delete --exclude-from=<exclude_list> <source> <destination>
rsync -aAXv --delete --stats --progress --info=progress2 --modify-window=1 --exclude-from=/home/cytopyge/.config/settings/rsync/parvus_exclude_list / /dock/1/parvus
rsync -aAXv --delete --stats --progress --info=progress2 --modify-window=1 --exclude-from=/home/cytopyge/settings_/rsync/parvus_exclude_list -e ssh / admin@192.168.3.199:/share/backup/devices/dellxps13
rsync -aAXv --delete <source> <destination>
rsync -aAXv --dry-run --delete --info=progress2 $XDG_DATA_HOME/c/ $XDG_DATA_HOME/b  ## backup_c2b
script; command; exit  ## make typescript of command
sed ':a;N;$!ba;s/\n//g' | sed 's/ //g' | sed 's/\r$//g'  ## remove all spaces from string
sed -i "$(( $( wc -l < $HISTFILE ) -1 )),\$d" $HISTFILE  ## remove history last line
sed -i '$d' <file>  ## remove last line from <file>
sed -i '/pttrn/d' file  ## delete all lines in file that contain 'pttrn'
sed -i '2,$d' <file>  ## remove all lines from line 2 from <file>
sed -i '2,3 d' <file>  ## remove line 2 and 3 from <file>
sed -i 'ni insert this into line n of file' <file>
sed -i 's/<find>/<replace>/g' <file>  ## replace <find> with <replace> in <file>
sed -i '/<find>/{n;s/.*/<replace>/}' <file>  ## find <find> and replace next line with <replace>
sed -i -n -e :a -e '1,2!{P;N;D;};N;ba' <file>  ## remove last 2 lines from <file>
sed -i '/^find/ s/./#&/' <file>  ## comment out lines containing 'find'
setfont ter-v32n
setopt interactive_comments  ## threat string after '#' as comment (unsetopt disables)
sh -xv file.sh  ## extra verbose debug output
showkey  ## [-a(scii), -s(cancodes), -k(eycodes)] examine codes sent by the keyboard
showkey -s
shred -v -z -n 3 -u <file>
simple-mtpfs --device 1 <mountpoint>  ## mount android device
sleep 3 && scrot -s
slurm -i <device>
sort $HISTFILE | uniq -c | sort -k1,1nr -k2 >> history_freq  ## create history frequency list
sort < $file | sort -nr > <file_sort_freq>
sort < $file | uniq -u > <file_uniq>
sort $file | uniq -c | sort -k1,1nr -k2 | sed 's/^ *[0-9]* //' > file_freq_sort
split $file -b N  ## split $file into files with N bytes
split $file -l N  ## split $file into files with N lines
split $file -n N  ## split $file into N files
ssh-keygen -t ed25519-sk -C $key_id -f $key_file  ## generate openssh key
ssh-keygen -R 192.168.0.195  ## remove cached key for ip on the local machine
ssh-keyscan -t ecdsa 192.168.x.y >> ~/.ssh/known_hosts  ## update ecdsa key for ip on local machine
stat -c %A <file>  ## display file access rights in human readable format
stat -c %a <file>  ## display file access rights in octal format
stat <file>  ## display file or file system status
stty -a  ## print all terminal line settings (human readable)
stty -g  ## print all tty settings (stty readable)
stty $(head -n 1 $XDG_CONFIG_HOME/tty/default)  ## (re)set default tty settings
sudo -k  ## invalidate cached credentials
sudo -l  ## list allowed commands for user
sudo -n echo >/dev/null 2>&1; echo $?  ## print sudo status
sudo ip link set up && sudo dhcpcd  && ping -c 1 9.9.9.9 && ip a
sudo iptables -A INPUT -s [ipaddr] -j DROP  ## add [ipaddr] to chain INPUT and drop all traffic
sudo rfkill unblock bluetooth && pulseaudio --start && sudo systemctl start bluetooth.service && bluetoothctl power on && bluetoothctl discoverable off ## bluetooth radio on
sudo systemctl start sshd
swaymsg -t get_outputs
echo '1' | sudo tee /proc/sys/kernel/sysrq  ## # obtain full sysrq priviledges
sysrq reisub  ## reboot
sysrq reisuo  ## shutdown
systemctl --failed
systemctl isolate graphical.target
systemctl isolate multi-user.target
systemctl list-unit-files
systemctl list-units
systemctl start vboxdrv vboxpci vboxnetadp vboxnetflt
systemd-analyze
tail -f /var/log/{messages,kernel,dmesg,syslog}
tac $file  ## concatenate and print file lines in reverse
tar -cv[j/z]f <archive.tar> -C <subdir> <files>  ## create tar archive [bzip2/gzip compression (optional)] files in archive relative to <subdir>
tar -cv[j/z]f <archive.tar> <files>  ## create tar archive [bzip2/gzip compression (optional)]
tar -tvf <archive.tar>  ## print contents of tar archive
tar -xv[j/z]f <archive.tar>  ## extract tar archive [bzip2/gzip compression (optional)]
tee -a file1 file2  ## append stdout to multiple files
tmux attach-session -t 0
tmux show-options -g
tput reset  ## reinitialize terminal state
tr -d '[:blank:]\t\r\n'  ## remove all whitespace
tr -s " " < file_with_double_spaces > file_without_double_spaces  ## remove double spaces
tr -d -c "[:alnum:]" < /dev/random | head -c 32  ## translate random stream in 32 alnum (alphanumeric) character class characters (generate passstring)
tr -d -c "[:graph:]" < /dev/random | head -c 32  ## translate random stream in 32 graph (printable without spaces) character class characters (generate passstring)
tr -d -c "[:alnum:]"'~!@#$%^&*()-=[];\,./_+{}:|<>?' < /dev/urandom | fold -w 32 | head -n 16 | cat -b  ## translate 16 random streams in 32 specified characters (generate passstring)
truncate -s 5M file  ## make file 5M size
tree -n --charset=unicode  ## depth indented file listing
type file or command  ## file or command interpretation
umount -l <mountpoint>  ## umount busy targets (mountpoints)
uptime
usermod -a -G group_2_add user_2_add  ## add user to group
vlock -a  ## console lock
watch -d -n 1 -t lsblk -i --tree -o name,fstype,uuid,path,size,fsuse%,fsused,label,mountpoint
watch -n 1 'ls -ilatr'
wev  ## show wayland events i.e. keypresses keynames
wget -rv -l 0 -nd http://proxpn.com/locations
which -a <command>  ## show path of all <command> (from $PATH)
while IFS= read -r pkg; do ls /var/cache/pacman/pkg | grep ^"$pkg"; done <<< "$(pacman -Qm | awk {print $1})" ## which package from Qm is in /v/c/p/p
while read -r -u $fd l; do <commands>; done {fd}< <file>  ## read file line by line into l (with stdin redirection)
while read l; do <commands>; done < <file>  ## read file line by line into l
whois -B 46.166.142.215  ## whois own public ip
wl-paste >> /home/cytopyge/.config/qutebrowser/bookmark_urls  ## add to bookmark_urls
wpa_supplicant -B -i wlp58s0 -c ~/keys/wl/mn.wifi
xargs -r command  ## run command with stdin as argument, no-run if empty
xxd -b -p <infile <outfile>> ## plain bits dump
xxd -p <infile <outfile>> ## plain hexdump
xxd -r -p <infile <outfile>> ## reverse plain hexdump
yay --gendb  ## generate developement package database
yay --show -w -w
yay -Gd $(yay -Qqm)  ## download package builds from installed aur packages
yay -P  ## show (is a yay specific [no pacman] option)
yay -Ps && yay --show -w && yay -Syu && yay -Rns $(yay -Qtdq) && yay -c && paccache -rv
yay -Pw  ## show news
yay -Q  ## query package database; display installed packages with version (Q=Qd+Qe=Qm+Qn)
yay -Qc <file>	## query package database; display changelog <file>
yay -Qd  ## (--deps) query package database; display packages installed as dependencies
yay -Qdt  ## query package database; display unrequired deps
yay -Qe  ## (--explicit) query package database; display explicitly installed packages
yay -Qent  ## query package database; display explicitly installed (no dependencies), not required packages that are in the package database
yay -Qg <group>  ## query package database; display packages that are member of <group>
yay -Qi <package>  ## query package database; display package infoos that are member of <group>cman
yay -Qk <package>  ## query package database; display number of total and missing files, check that all files of the package are present on the system
yay -Ql <package>  ## query package database; display package file list
yay -Qm  ## (--foreign) query package database; display packages that are not in the sync database
yay -Qmq  ## (--foreign) query package database; display packages that are not in the sync database no version
yay -Qn  ## (--native) query package database; display packages that are in the sync database
yay -Qnq | yay -S -  ## reinstall native packages
yay -Qo <file>	## query package database; display which package owns <file>
yay -Qq ## query package database; display installed packages (raw)
yay -Qql <package>  ## query package database; display package file list (raw)
yay -R [package] OR [group]  ## remove [package] OR [group]
yay -Rns [package]  ## remove [package] including not required and not explicitly installed dependencies, no pacsave
yay -S [package]  ## synchronize package; download and install
yay -Sc [package]  ## remove all cached packages that are currently not installed (more agressive than paccache)
yay -Scc [package]  ## remove all cached packages (far more agressive than paccache, will prevent downgrading or reinstalling without downloading)
yay -Syu --devel --timeupdate  ## update, also check AUR developement packages (git), timeupdate (instead of version number)
yay -Tv  ## check dependencies; show file paths
yay -U <package>  ## remove then upgrade or add package
ykman otp static --no-enter --keyboard-layout US 2 'string' ## assign string to yk slot 2
zsh --no-rcs  ## zsh terminal without run command script
chmod 0000	## ---------- 		ugo -	no permissions
chmod 0111	## ---x--x--x 		ugo x				execute
chmod 0222	## --w--w--w- 		ugo w			write
chmod 0333	## --wx-wx-wx 		ugo wx			write	execute
chmod 0444	## -r--r--r-- 		ugo r	read
chmod 0555	## -r-xr-xr-x 		ugo xr	read			execute
chmod 0666	## -rw-rw-rw- 		ugo	rw	read	write
chmod 0777	## -rwxrwxrwx 		ugo	rwx	read	write	execute
chmod 0700	## -rwx------ 		u	rwx	g	-	o	-
chmod 0740	## -rwxr----- 		u	rwx	g	r	o	-
chmod 0770	## -rwxrwx--- 		u	rwx	g	rwx	o	-
case $expr in pattern1) echo 1 ;; ptrn2) echo 2 ;; *) echo 3 ;; esac  ## case statement basic structure
if [[ $i -eq 0 ]]; then; echo 0; elif [[ $i -lt 0 ]]; then; echo neg; else; echo pos; fi  ## if elif else statement basic structure
for i in 1 2 3; do; echo $i; done  ## for loop basic structure
for (( i=1; i<=10; i++ )); do echo $i; done  ## for loop alternative structure
select option in 'a' 'b' 'c'; do echo "selection: $option"; break; done  ## select basic structure
while [[ $i -lt 3 ]]; do echo $i; ((i+=1)); done  ## while loop basic structure
until [[ $i -gt 3 ]]; do echo $i; ((i+=1)); done  ## until loop basic structure
reset               # clear + reset internal terminal state + 1sec delay
tput reset          # same as reset but without 1sec delay
stty sane           # don't clear screen but reset some terminal options
echo -e "\033c"     # same as tput reset but hardcoded escape seq
printf "\033c"      # same as tput reset but hardcoded escape seq
setterm -reset      # same as tput reset, setterm has friendlier commands
DELtest
